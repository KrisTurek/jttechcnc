<?php

//if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {
//    die('Sorry Request must be Ajax POST'); //exit script
//}

require('recaptcha-master/src/autoload.php');

$from = 'turek.krzysztof.230185@gmail.com';
$sendTo = 'turek.krzysztof.230185@gmail.com';
$subject = 'Sales Inquiry - jttechcnc.com';
$fields = array('name' => 'Name', 'phone' => 'Phone', 'email' => 'Email', 'message' => 'Message');
$okMessage = 'Message successfully submitted.';
$errorMessage = 'There was an error while submitting the form. Please try again later';
$recaptchaSecret = '6Lc4VR0UAAAAAJ-GAMFEhuJVe8NxLQRpL11QmC8n';
$responseArray = array();

try {
    //dump_to_log($_REQUEST);
    //dump_to_log($_POST);
    //dump_to_log($_FILES);

    check_file_size();

    if ($_POST) {
        if (!isset($_POST['g-recaptcha-response'])) {
            throw new \Exception('ReCaptcha is not set.');
        }

        $recaptcha = new \ReCaptcha\ReCaptcha($recaptchaSecret, new \ReCaptcha\RequestMethod\SocketPost());
        $response = $recaptcha->verify($_POST['g-recaptcha-response'], $_SERVER['REMOTE_ADDR']);

        if (!$response->isSuccess()) {
            $error = '';
            foreach ($response->getErrorCodes() as $code) {
                $error .= "$code, ";
            }
            throw new \Exception($error);
        }

        $message = "You have new message from contact form\n=============================\n";
        $message .= "Name: " . $_POST['salesInquiryName'] . "\n";
        $message .= "Email: " . $_POST['salesInquiryEmail'] . "\n";
        $message .= "Phone: " . $_POST['salesInquiryPhone'] . "\n";
        $message .= "Message: " . $_POST['salesInquiryMessage'] . "\n";

        $attachments = $_FILES['file_attach'];
        $boundary = md5("jttechcnc.com");
        $body = '';
        if (isset($attachments) && $attachments['error'] == 0) {
            $headers = "MIME-Version: 1.0\r\n";
            $headers .= "From:" . $from . "\r\n";
            $headers .= "Reply-To: " . $from . "" . "\r\n";
            $headers .= "Content-Type: multipart/mixed; boundary = $boundary\r\n\r\n";


            $body = "--$boundary\r\n";
            $body .= "Content-Type: text/plain; charset=ISO-8859-1\r\n";
            $body .= "Content-Transfer-Encoding: base64\r\n\r\n";
            $body .= chunk_split(base64_encode($message));

            if (!empty($attachments['name'])) {
                $file_name = $attachments['name'];
                $file_size = $attachments['size'];
                $file_type = $attachments['type'];

                $handle = fopen($attachments['tmp_name'], "r");
                $content = fread($handle, $file_size);
                fclose($handle);
                $encoded_content = chunk_split(base64_encode($content));

                $body .= "--$boundary\r\n";
                $body .= "Content-Type: $file_type; name=" . $file_name . "\r\n";
                $body .= "Content-Disposition: attachment; filename=" . $file_name . "\r\n";
                $body .= "Content-Transfer-Encoding: base64\r\n";
                $body .= "X-Attachment-Id: " . rand(1000, 99999) . "\r\n\r\n";
                $body .= $encoded_content;
            }
        } else if (isset($attachments) && $attachments['error'] != 4) {
            $mymsg = array(
                1 => "The uploaded file exceeds the upload_max_filesize directive in php.ini",
                2 => "The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form",
                3 => "The uploaded file was only partially uploaded",
                4 => "No file was uploaded",
                6 => "Missing a temporary folder");
            throw new \Exception($mymsg[$attachments['error']]);
        } else {
            $body .= $message;
            $headers = implode("\n", array('Content-Type: text/plain; charset="UTF-8";',
                'From: ' . $from,
                'Reply-To: ' . $from,
                'Return-Path: ' . $from,
            ));
        }
        mail($sendTo, $subject, $body, $headers);
        $responseArray = array('type' => 'success', 'message' => $okMessage);
    }
} catch (\Exception $e) {
    $responseArray = array('type' => 'danger', 'message' => $e->getMessage());
}

if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
    $encoded = json_encode($responseArray);
    header('Content-Type: application/json');
    echo $encoded;
} else {
    echo $responseArray['message'];
}

function dump_to_log($param) {
    $dump = print_r($param, TRUE);
    $fp = fopen('dump.log', 'a');
    fwrite($fp, $dump);
    fclose($fp);
}

function check_file_size() {
    if ($_SERVER['REQUEST_METHOD'] == 'POST' && empty($_POST) && empty($_FILES) && $_SERVER['CONTENT_LENGTH'] > 0) {
        $displayMaxSize = ini_get('post_max_size');

        switch (substr($displayMaxSize, -1)) {
            case 'G':
                $displayMaxSize = $displayMaxSize * 1024;
            case 'M':
                $displayMaxSize = $displayMaxSize * 1024;
            case 'K':
                $displayMaxSize = $displayMaxSize * 1024;
        }

        $error = 'Posted data is too large. ' .
                $_SERVER['CONTENT_LENGTH'] .
                ' bytes exceeds the maximum size of ' .
                $displayMaxSize . ' bytes.';
        throw new \Exception($error);
    }
}
